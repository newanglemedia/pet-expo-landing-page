;(function(){


	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

		function secondaryHeaderLoader(){
			var loader = new TimelineMax({});
			loader.call(function() { init(); animate(); }, null, null, '-=0')
			.call(function() { $('#inner-body').css('visibility','visible'); }, null, null, '-=0')
			.from('#home-hero h1', 1, {bottom:200, opacity:0}, '-=0')
		}		
		secondaryHeaderLoader();

	}else{      	
		
		if(sessionStorage.getItem('NAMHompageLoader') !== 'true'){
			firstTimeLoad();
			sessionStorage.setItem('NAMHompageLoader', 'true');
		}else{			
			secondaryHeaderLoader();
		}

		
		var controller = new ScrollMagic.Controller();  

		//Hero Parallax
		var hero = new TimelineMax({})
		.to('#home-hero .pop-out', 1, {height:'100%'}, "0")
		.from('#home-video', 1	, {marginTop:200}, "-=1")
		.from('#home-video .pop-frame', 1	, {top:100}, "-=1")
		.from('#home-intro', 1	, {paddingTop:550}, "-=1")
		.to('#home-hero .section-arrow', 1, {height:600}, "-=.5");
		var heroScene = new ScrollMagic.Scene({duration: '100%', triggerElement:'#home-hero', triggerHook:0 })
		.setTween(hero)
		.addTo(controller);    
		

		//Project Build-in
		$('#home-projects > ul > li').each(function() {   
			var projects = new ScrollMagic.Scene({triggerElement:this, triggerHook:0.6})
			.setTween(new TimelineMax ()
				.from($(this), .5, {opacity:0, right:-20}, "0")
				//.from($(this).find('.fg'), .5, {scale:1.3}, "-=.5")
				.from($(this).find('.info'), .5, {bottom:-40, opacity:0}, '-=.5')
				.from($(this).find('.logo'), .5, {top:-40, opacity:0}, '-=.5')
			)
			.addTo(controller);      
		});  

		//Services Build-in
		$('#home-services > li ').each(function() {   
			var services = new ScrollMagic.Scene({triggerElement:this})
			.setTween( new TimelineMax ()
				.from($(this), .5, {opacity:0})
				.to($(this), .5, {color:'#26acff'})
			)
			.addTo(controller);      
		});  

		//Footer Build-in
		var testimonials = new TimelineMax({})
		.from('#home-testimonials', 1, {opacity:0, ease: Quad.easeOut})
		.from('#home-testimonials img', 1, {top:150, paddingLeft:50, ease: Quad.easeOut}, '-=1')
		.from('#home-testimonials .info', 1, {y:-100, ease: Quad.easeOut}, '-=1');

		var testimonialsScene = new ScrollMagic.Scene({triggerElement:'#home-testimonials', triggerHook:.8})
		.setTween(testimonials)
		.addTo(controller);    
		
		//home-blogs
		var blogs = new TimelineMax({})
		.from('#home-blogs h3', 1, {top:150, opacity:0}, "-=1")
		.from('#home-blogs li', 1, {top:150, opacity:0}, "-=.5");

		var blogsScene = new ScrollMagic.Scene({triggerElement:'#home-blogs', triggerHook:.8})
		.setTween(blogs)
		.addTo(controller);    

		//Logo Animation
		function pathPrepare ($el) {
			var lineLength = $el[0].getTotalLength();
			$el.css("stroke-dasharray", lineLength);
			$el.css("stroke-dashoffset", lineLength);
		}

		var $word = $("path#word");
		pathPrepare($word);

		var controller = new ScrollMagic.Controller();

		var tween = new TimelineMax()
		.add(TweenMax.to($word, 3, {strokeDashoffset: 0, ease:Linear.easeNone}));

		var scene = new ScrollMagic.Scene({tweenChanges: true, triggerElement:'#home-intro'})
		.setTween(tween)
		.addTo(controller);  


		function rotateLoaderLogo() {
			var rotate = new TimelineMax({repeat:-1}).to('.loader', 0, {rotation:0, ease:Circ.easeOut}, '+=0').to('.loader', 1, {rotation:360, ease:Circ.easeInOut}, '+=0');
		}

		function firstTimeLoad(){
			var loader = new TimelineMax({});
			loader.call(function() { $('#inner-body').css('visibility','visible'); $('.loader').show(); rotateLoaderLogo(); }, null, null, '-=0')
			.set('body', {position:'fixed', left:0, right:0})
			.from('.container', 1, {delay: 3, maxWidth:'100%', paddingLeft:0, paddingRight:0, ease:Circ.easeOut}, '+=0')
			.call(function() { $('.loader').hide();  }, null, null, '-=1')
			.from('body', 1, {paddingTop:0, ease:Circ.easeOut}, '-=0')
			.from('header', 1, {top:'-100%', ease:Circ.easeOut}, '-=1')		
			.from('#home-hero .bg', 1, {height:'150%'}, '-=1.5')
			.call(function() { init(); animate(); }, null, null, '-=.5')
			.from('#home-hero h1', 1, {bottom:200, opacity:0}, '-=1')
			.from('#home-hero .section-arrow', .5, {height:0, ease:Circ.easeOut}, '-=.5')
			loader.set('body', {position:'relative'})
		}	

		function secondaryHeaderLoader(){
			var loader = new TimelineMax({});
			loader.call(function() { $('#inner-body').css('visibility','visible'); }, null, null, '-=0')
			.set('body', {position:'fixed', left:0, right:0})
			.call(function() { init(); animate(); }, null, null, '-=0')
			.from('#home-hero h1', 1, {bottom:200, opacity:0}, '-=0')
			.from('#home-hero .section-arrow', .5, {height:0, ease:Circ.easeOut}, '-=.5')
			loader.set('body', {position:'relative'});

		}
	}

	var particlesConfig = {
	  "particles": {
	    "number": {
	      "value": 180,
	      "density": {
	        "enable": true,
	        "value_area": 473.50726833656915
	      }
	    },
	    "color": {
	      "value": "#ffffff"
	    },
	    "shape": {
	      "type": "circle",
	      "stroke": {
	        "width": 0,
	        "color": "#000000"
	      },
	      "polygon": {
	        "nb_sides": 5
	      },
	      "image": {
	        "src": "img/github.svg",
	        "width": 100,
	        "height": 100
	      }
	    },
	    "opacity": {
	      "value": 1,
	      "random": true,
	      "anim": {
	        "enable": true,
	        "speed": 1,
	        "opacity_min": 0,
	        "sync": true
	      }
	    },
	    "size": {
	      "value": 1.3,
	      "random": true,
	      "anim": {
	        "enable": false,
	        "speed": 4,
	        "size_min": 0.3,
	        "sync": false
	      }
	    },
	    "line_linked": {
	      "enable": false,
	      "distance": 150,
	      "color": "#ffffff",
	      "opacity": 0.4,
	      "width": 1
	    },
	    "move": {
	      "enable": true,
	      "speed": 1,
	      "direction": "bottom-right",
	      "random": true,
	      "straight": false,
	      "out_mode": "out",
	      "bounce": false,
	      "attract": {
	        "enable": false,
	        "rotateX": 600,
	        "rotateY": 600
	      }
	    }
	  },
	  "interactivity": {
	    "detect_on": "canvas",
	    "events": {
	      "onhover": {
	        "enable": false,
	        "mode": "bubble"
	      },
	      "onclick": {
	        "enable": false,
	        "mode": "repulse"
	      },
	      "resize": true
	    },
	    "modes": {
	      "grab": {
	        "distance": 400,
	        "line_linked": {
	          "opacity": 1
	        }
	      },
	      "bubble": {
	        "distance": 250,
	        "size": 0,
	        "duration": 2,
	        "opacity": 0,
	        "speed": 3
	      },
	      "repulse": {
	        "distance": 400,
	        "duration": 0.4
	      },
	      "push": {
	        "particles_nb": 4
	      },
	      "remove": {
	        "particles_nb": 2
	      }
	    }
	  },
	  "retina_detect": true
	};
	particlesJS('particles-js', particlesConfig);

	var SCREEN_WIDTH = document.getElementById('home-hero').clientWidth, SCREEN_HEIGHT = document.getElementById('home-hero').clientHeight;
	var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 20000;

	var scene, camera, renderer, controls, floor;

	var simplex = new SimplexNoise();

	var delta = 0;

	var floorGeometry;

	var didScroll = false;
	var pastScroll = 0;
	var curSroll = 0;

	var raycaster;
	var mouseVec = new THREE.Vector2();
	var intersection = null;

	$("#home-hero").mousemove(function(e){
		onMouseMove($(this), e);
	});
	$("#home-hero").on("touchmove", function(e){
		e.preventDefault();
		console.log(e);
	});


	var init = function(){
		scene = new THREE.Scene();
		
		camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);
		camera.position.set(0,-75,-265);
		camera.rotation.set(0,0,0);

		renderer = new THREE.WebGLRenderer( {antialias:true, alpha:true} );
		renderer.setClearColor(0x000000, 0);
		renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
		renderer.sortObjects = false;

		raycaster = new THREE.Raycaster();

		var header = document.getElementById('headerScene');
		header.appendChild( renderer.domElement );

		// LIGHT
		var hemiLight = new THREE.HemisphereLight(0x000000, 0xffffff, 0.95);
		hemiLight.position.set(0, 2000, -100);
		scene.add(hemiLight);

		var light = new THREE.AmbientLight(0xffffff, 0.9);
		light.position.set(0,100,200);
		scene.add(light);

		// FLOOR
		var floorMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff,  wireframe: true, transparent: false, vertexColors: THREE.FaceColors } );
		floorGeometry = new THREE.PlaneGeometry(450, 450, 30, 20);

		floor = new THREE.Mesh(floorGeometry, floorMaterial);
		floor.position.y = -150;
		floor.rotation.x = 29.97;
		scene.add(floor);

		var t0 = new TWEEN.Tween( camera.position ).to( {
		  x: camera.position.x,
		  y: camera.position.y,
		  z: 265,
		}, 1000 )
		.easing( TWEEN.Easing.Sinusoidal.Out)
		.onComplete(function(){

			setTimeout(function(){
				for(var i = 0; i < (floorGeometry.vertices.length * .95); i++){
					var v = floorGeometry.vertices[i];
					var v2 = {};
					v2.z = getNoise(v.x * 0.01, v.y * 0.01, v.z * 0.01, 0) * 30;
					v2.z += getNoise(v.x * 0.03, v.y * 0.03, v.z * 0.03, 0) * 10;
					v2.z += getNoise(v.x * 0.1, v.y * 0.125, v.z * 0.125, 0) * 20;

				    new TWEEN.Tween( floorGeometry.vertices[i] ).to( {
				      x: v.x,
				      y: v.y,
				      z: v2.z,
				    }, 5500 )
				    .easing( TWEEN.Easing.Elastic.Out).start();
				}
				
			},100);

		});

		setTimeout(function(){
			t0.start();
		}, 0);
	}

	var animate = function(){
		requestAnimationFrame( animate );
		update();
		render();
	}

	var update = function(){
		delta += 0.05;

		TWEEN.update();
		floorGeometry.verticesNeedUpdate = true;
		floorGeometry.colorsNeedUpdate = true;
	}

	var render = function () {
		renderer.render(scene, camera);
	};

	var getNoise = function(x, y, z, t){
		return simplex.noise3D(x, y, z, t);
	}

	function onMouseMove(t,e){
	   var parentOffset = t.parent().offset(); 
	   //or $(this).offset(); if you really just want the current element's offset
	   var relX = e.pageX - parentOffset.left;
	   var relY = e.pageY - parentOffset.top;

		mouseVec.x = 2 * (relX / SCREEN_WIDTH) - 1;
		mouseVec.y = 1 - 2 * ( relY / SCREEN_HEIGHT );

		raycaster.setFromCamera( mouseVec, camera );

		var intersections = raycaster.intersectObjects( scene.children );
		intersection = ( intersections.length ) > 0 ? intersections[ 0 ] : null;
		if(intersection !== null){
			var geo = intersection.object.geometry;
			var face = geo.faces[intersection.faceIndex];


			setTimeout(function(){

				face.color.setHex( 0xf36d34 );
			    new TWEEN.Tween( face.color ).to( {
			      r: 1,
			      g: 1,
			      b: 1,
			    }, 8500 )
			    .easing( TWEEN.Easing.Elastic.Out).start();

				var v = geo.vertices[face.a];
				var v2 = {};
				v2.z = getNoise(v.x * 0.01, v.y * 0.01, v.z * 0.01, 0) * 30;
				v2.z += getNoise(v.x * 0.03, v.y * 0.03, v.z * 0.03, 0) * 10;
				v2.z += getNoise(v.x * 0.1, v.y * 0.125, v.z * 0.125, 0) * 20;

			    new TWEEN.Tween( geo.vertices[face.a] ).to( {
			      x: v.x,
			      y: v.y,
			      z: v2.z,
			    }, 5500 )
			    .easing( TWEEN.Easing.Elastic.Out).start();

			},10);
		}
	}


})();





$("#js-rotating").Morphext({
    // The [in] animation type. Refer to Animate.css for a list of available animations.
    animation: "bounceIn",
    // An array of phrases to rotate are created based on this separator. Change it if you wish to separate the phrases differently (e.g. So Simple | Very Doge | Much Wow | Such Cool).
    separator: ",",
    // The delay between the changing of each phrase in milliseconds.
    speed: 2000,
    complete: function () {
        // Called after the entrance animation is executed.
    }
});