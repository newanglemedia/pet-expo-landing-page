var $grid = $('#agency-services > .grid').imagesLoaded( function() {
	$grid.masonry({
		itemSelector: '.grid-item',
		columnWidth: '.grid-sizer',
		percentPosition: true		
	});
});	

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {}
else {      	
	
	var controller = new ScrollMagic.Controller();  

	//Hero Parallax
	var hero = new TimelineMax({})
	.to('#banner .pop-out', 1, {height:'100%'}, "0")
	.from('#agency-intro .pop-frame', 1	, {top:100}, "-=1")
	.to('#banner .section-arrow', 1, {height:800}, "-=1");
	var heroScene = new ScrollMagic.Scene({duration: 2000})
	.setTween(hero)
	.addTo(controller);    


	//Project Build-in
	$('#agency-services .grid-item').each(function() {   
		var projects = new ScrollMagic.Scene({triggerElement:this, triggerHook:.75})
		.setTween(new TimelineMax ()
			.from($(this).find('span'), .5, {opacity:0, left:-20})
			.from($(this).find('h3'), .5, {opacity:0, left:20}, '-=.5')
			.from($(this).find('p'), .25, {opacity:0}, '-=.75')
			.from($(this).find('a'), .25, {opacity:0}, '-=.75')
		)
		.addTo(controller);      
	});  
	
	//Team
	$('#agency-team > div').each(function() {   
		var left = Math.floor(Math.random() * 100) + 1;
		var bottom = Math.floor(Math.random() * -100) + 1;
		
		var projects = new ScrollMagic.Scene({triggerElement:this, triggerHook:.75})
		.setTween(new TimelineMax ()
			.from($(this), 1, {opacity:0, left:left, bottom:bottom})
		)
		.addTo(controller);      
	});  
	
	var partners = new TimelineMax({})
	.from('#agency-partners', 1.5, {x:-200}, "0")
	.from('#agency-partners', .25, {opacity:0}, "-=1.5")
	.from('#agency-partners h3', .75, {x:-200, opacity:0}, "-=1.2")
	.from('#quote', 1.5, {x:-600, ease: Quint.easeOut}, '-=1')
	.from('#agency-partners span', .5, {height:0}, '-=1')
	var heroScene = new ScrollMagic.Scene({triggerElement:'#agency-partners', offset: -200})
	.setTween(partners)
	.addTo(controller);   
	
	


}


var tween;

$('.headshot').hover(function() {
	
	var hs = $(this).find('img');
// define images
	var images = [
		"headshots-1",
		"headshots-2",
		"headshots-3",
		"headshots-4",
		"headshots-5",
		"headshots-6",
		"headshots-7"


	];
	
	// TweenMax can tween any property of any object. We use this object to cycle through the array
	var obj = {curImg: 0};

	// create tween
	tween = TweenMax.to(obj, 0.5,
		{
			curImg: images.length - 1,	// animate propery curImg to number of images
			roundProps: "curImg",				// only integers so it can be used as an array index
			yoyo:true,
			repeat: -1,									// repeat 3 times
			immediateRender: true,			// load first image automatically
			ease: Linear.easeNone,			// show every image the same ammount of time
			onUpdate: function () {
			  $(hs).attr("class", images[obj.curImg]); // set the image source
			}
		}
	);
}, function() {

	tween.kill();
}
);





//Youtube Lazy Loader
document.addEventListener('DOMContentLoaded',
    function() { console.log('DOMContentLoaded');
        var div, n,
            v = document.getElementsByClassName('youtube-player');
        for (n = 0; n < v.length; n++) {
            div = document.createElement('div');
            div.setAttribute('data-id', v[n].dataset.id);
            div.innerHTML = buildYoutubeThumb(v[n].dataset.id);
            div.onclick = buildYoutubeIframe;
            v[n].appendChild(div);
        }
    }); 

function buildYoutubeThumb(id) {
    var thumb = '<img src=\"https://i.ytimg.com/vi/ID/maxresdefault.jpg\">',
        play = '<div class=\"play\"></div>';
    return thumb.replace('ID', id) + play;
}

function buildYoutubeIframe() {
    var iframe = document.createElement('iframe');
    var embed = 'https://www.youtube.com/embed/ID?autoplay=1&rel=0';
    iframe.setAttribute('src', embed.replace('ID', this.dataset.id));
    iframe.setAttribute('frameborder', '0');
    iframe.setAttribute('allowfullscreen', '1');
    this.parentNode.replaceChild(iframe, this);
}
