$(document).ready(function() {

    var controller;
    controller = new ScrollMagic();     
    
                    
    var stickyNav = new TimelineMax()
    .add([
        TweenMax.from('#header .bg', .5, { css:{opacity:0}}),
        TweenMax.from('#header', .5, { css:{borderColor:'transparent'}}),
        TweenMax.from('#header *', .5, { css:{color:'#fff'}})
                          
    ]); 
    new ScrollScene({offset:50})
    .setTween(stickyNav) 
    .addTo(controller);      
       
    $("#lobbyExhibitors ul").owlCarousel({
            items : 6,
            autoPlay: true,
            itemsDesktop : [1200,6],            
            itemsDesktopSmall : [996,5],            
            itemsTablet : [768,4],            
            itemsTabletSmall : [680,3], 
            itemsMobile : [500,2],      
            navigation : true,
            navigationText : ["",""],
            pagination:false
      
    });

   
     if ($(window).width() > 768) {

        var wh = $(window).height();
        var pitchHeight = $('#lobbyHero .container > div').height();
        var pitchMargin = ((wh/2 - pitchHeight/2));
        $('#lobbyHero .container > div').css({'padding-top':pitchMargin});  
        $('#lobbyHero .container > div').show();          
     }
     
});

$(window).load(function() {   
        var heights = $("#features .row > div").map(function() {
            return $(this).height();
        }).get(),
        maxHeight = Math.max.apply(null, heights);
        $("#features .row > div").height(maxHeight);
});
       