$( document ).ready(function() {
	var $grid = $('#projects-featured > .grid').imagesLoaded( function() {
		$grid.masonry({
			itemSelector: '.grid-item',
			columnWidth: '.grid-sizer',
			percentPosition: true		
		});
	});	
});	

$('.fotorama').click(function(event) {
	event.stopPropagation();
});




$('#projects-archive .collection > li').click(function() {
	var foto = $(this).find('.fotorama');
	$(this).siblings().removeClass('active');
	$(this).toggleClass('active');

	var imgs = $(this).data('imgs');

	$.each( imgs, function( i, l ){
		if(imgs[i].type !== undefined && imgs[i].type == "video"){
			var img = '<a href="'+imgs[i].src+'"></a>';
			foto.append(img);
		}else{
			var img = '<img src="'+imgs[i].src+'" alt="'+imgs[i].caption+'" data-caption="'+imgs[i].caption+'" />';
			foto.append(img);
		}
	});

	 $(this).find('.fotorama').fotorama();
});	


if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {}
else {      	
	
$( document ).ready(function() {
	
	var controller = new ScrollMagic.Controller();  


	//Hero Parallax
	var hero = new TimelineMax({})
	.to('#banner .pop-out', 1, {height:'100%'}, "0")
	.from('#agency-intro .pop-frame', 1	, {top:100}, "-=1")
	.to('#banner .section-arrow', 1, {height:500}, "-=1");
	var heroScene = new ScrollMagic.Scene({duration: 2000})
	.setTween(hero)
	.addTo(controller);    

	
	

	var height;
	var year;
	
	$('.year').each(function() {  
		height = $(this).height();
		year = $(this).parent().find('.year');		
		var scene = new ScrollMagic.Scene({triggerElement: this, triggerHook: 0, duration:height, offset:-160})
		.setPin(this) 
		.addTo(controller);
	});  
	

/*
	$('#projects-archive .collection > li').each(function() {   
		var projects = new ScrollMagic.Scene({triggerElement:this, triggerHook:.5, duration:300})
		.setTween(new TimelineMax ()
			.to($(this), .5, {backgroundColor:'#eee'})
			.to($(this), .5, {backgroundColor:'#fff'}, '+=2')
		)
		.addTo(controller);      
	});  
*/


	//Team
/*	$('.grid-item').each(function() {   
		var left = Math.floor(Math.random() * 100) + 1;
		var bottom = Math.floor(Math.random() * -100) + 1;
		
		var projects = new ScrollMagic.Scene({triggerElement:this, triggerHook:.75})
		.setTween(new TimelineMax ()
			.from($(this), 1, {opacity:0, left:left, bottom:bottom})
		)
		.addTo(controller);      
	});  */

});

}
